![截图](./icon.png)
# tkinter-start

#### 介绍
Python Tkinter GUI 快速开始，Pyinstaller打包exe

#### 软件截图
![截图](./screenshot/1.png)

#### 安装依赖 / 运行

- `pip install tkinter`
- `python icon2py.py`
- `python main.py`

#### 打包构建 / 清理

- `pip install pyinstaller`
- `build.bat`
- `clean.bat`

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request