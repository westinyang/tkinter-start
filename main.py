#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import time
import base64
from icon import img
import tkinter as tk
import tkinter.messagebox
import tkinter.filedialog
from tkinter.scrolledtext import ScrolledText

class MainScene():
    # 显示日志时间
    show_log_time = False

    # 构造函数
    def __init__(self):
        # 创建主窗口
        self.win = tk.Tk()
        self.win.resizable(width=False, height=False)
        self.win.title('Tkinter Start')
        # self.win.iconbitmap('icon.ico')
        
        # 使用icon.py设置图标，兼容pyinstaller打包
        tmp = open("tmp.ico","wb+")
        tmp.write(base64.b64decode(img))
        tmp.close()
        self.win.iconbitmap("tmp.ico")
        os.remove("tmp.ico")

        # 初始化窗口大小，居中显示
        self.init_position(self.win, 480, 320)
        # 初始化组件
        self.init_component()
        self.win.mainloop()

    # 初始化窗口大小，居中显示
    def init_position(self, root, curWidth='', curHight=''):
        '''
        设置窗口大小，并居中显示
        :param root:主窗体实例
        :param curWidth:窗口宽度，非必填，默认200
        :param curHight:窗口高度，非必填，默认200
        :return:无
        '''
        if not curWidth:
            '''获取窗口宽度，默认200'''
            curWidth = root.winfo_width()
        if not curHight:
            '''获取窗口高度，默认200'''
            curHight = root.winfo_height()
        # print(curWidth, curHight)

        # 获取屏幕宽度和高度
        scn_w, scn_h = root.maxsize()
        # print(scn_w, scn_h)

        # 计算中心坐标
        cen_x = (scn_w - curWidth) / 2
        cen_y = (scn_h - curHight) / 2
        # print(cen_x, cen_y)

        # 设置窗口初始大小和位置
        size_xy = '%dx%d+%d+%d' % (curWidth, curHight, cen_x, cen_y)
        root.geometry(size_xy)

    # 初始化组件
    def init_component(self):
        # 按钮组
        self.button1 = tk.Button(self.win, text = "提示框", command = self.button1_click).place(x=5, y=5, width=80, height=30)
        self.button2 = tk.Button(self.win, text = "询问框", command = self.button2_click).place(x=90, y=5, width=80, height=30)
        self.button3 = tk.Button(self.win, text = "选择文件", command = self.button3_click).place(x=175, y=5, width=80, height=30)
        self.button4 = tk.Button(self.win, text = "选择目录", command = self.button4_click).place(x=260, y=5, width=80, height=30)
        self.button5 = tk.Button(self.win, text = "清空日志", command = self.button5_click).place(x=395, y=5, width=80, height=30)
        # 文本域
        self.text = ScrolledText(self.win)
        self.text.configure(state = tkinter.DISABLED)
        self.text.place(x=5, y=40, width=470, height=275)

    # 追加日志
    def append_log(self, str):
        self.text.configure(state = tkinter.NORMAL)
        final_str = str + "\n"
        if (self.show_log_time):
            final_str = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()) + final_str
        self.text.insert(tk.END, final_str)
        self.text.see(tk.END)
        self.text.configure(state = tkinter.DISABLED)

    # 清空日志
    def clean_log(self):
        self.text.configure(state = tkinter.NORMAL)
        self.text.delete(0.0, tk.END)
        self.text.configure(state = tkinter.DISABLED)

    # 按钮1事件
    def button1_click(self):
        tk.messagebox.showinfo("提示", "提示信息")
        
    # 按钮2事件
    def button2_click(self):
        b = tk.messagebox.askokcancel("提示", "确认要进行此操作吗？")
        if b:
            self.append_log("询问框：你点击了确认")
    
    # 按钮3事件
    def button3_click(self):
        # file = tk.filedialog.askopenfile()   # 返回文件流
        filename = tk.filedialog.askopenfile()   # 返回文件名
        if filename:
            self.append_log("选择文件：" + filename.name)

    # 按钮4事件
    def button4_click(self):
        dirname = tk.filedialog.askdirectory()   # 返回文件名
        if dirname:
            self.append_log("选择目录：" + dirname)

    # 按钮5事件
    def button5_click(self):
        self.clean_log()


if __name__ == '__main__':
	MainScene()